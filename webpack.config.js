const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = {
  entry: {
    script: './kirui/index.js',
    style: './assets/scss/bootstrap.scss'
  },
  output: {
    filename: '[name].js',
    // path: path.resolve(__dirname, 'kirui_devserver/static/django_kirui'),
    path: '/home/lovasb/WORK/NEXON/polip/static/django_kirui',
    library: {
      name: 'kirui',
      type: 'window'
    }
  },
  mode: "development",
  watch: true,
  watchOptions: {
    aggregateTimeout: 200,
    poll: 1000,
    ignored: '**/node_modules'
  },
  plugins: [new MiniCssExtractPlugin({filename: '[name].css'})],
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(scss|css)$/,
        use: [
            MiniCssExtractPlugin.loader,
            "css-loader",
            "sass-loader"
        ],
      }
    ]
  },
  resolve: {
    alias: {
     'react': 'preact/compat',
     'react-dom': 'preact/compat',
    },
  }
};
