import {Component as PreactComponent, h, render} from 'preact';


class Form extends PreactComponent {
        constructor() {
                super();
        }

        render(props, state, context) {
                return <div>valami{this.props.children}</div>;
        }
}

class Field extends PreactComponent {
        constructor(props) {
                super(props);

                if (this.props.onChange) {
                        this.props.onChange = eval(this.props.onChange);
                }

                this.handleInputChange = this.handleInputChange.bind(this);
        }

        handleInputChange(ev) {
                console.log('itt')
        }

        render(props, state, context) {
                return <input type="text" value="" name="elso" {...this.props} />;
        }
}

const element = h(Form, null, [
        h(Field, {onChange: "{this.handleInputChange}" }, []),
        h(Field, {onChange: "{this.handleInputChange}" }, []),
])

render(element, document.body);