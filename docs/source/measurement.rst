Performance measurements
------------------------

.. code-block:: python

    import datetime

    from django_brython.assets import require
    from django_brython.testing import location

    from browser import document, webcomponent, window

    from django_brython.testing import reverse
    from kirui.vdom.element import VElement


    class CustomElement:
        def connectedCallback(self):
            print('connectedCallback')


    webcomponent.define('custom-element', CustomElement)


    class BrythonTest:
        @location(reverse('backend:index'), wait_for=lambda: document.getElementById("main") is not None)
        def test_example(self):
            count = 10000

            xml = require('big_html.html')

            print(f'Creating {count} elements with native createElement function')
            start = datetime.datetime.now()
            print('    Started: ', start)
            for i in range(0, 10000):
                el = document.createElement('custom-element')
                for j in range(0, 10):
                    el.setAttribute(f'data-{j}', j)

                for j in range(0, 10):
                    sub_el = document.createElement('p')
                    el.appendChild(sub_el)

            end = datetime.datetime.now()
            print('    Ended: ', end)
            print('    Duration: ', (end - start).total_seconds(), 'seconds')

            print(f'Creating {count} elements with VElement initialization')
            start = datetime.datetime.now()
            print('    Started: ', start)
            for i in range(0, 10000):
                attrs = {}
                for j in range(0, 10):
                    attrs[f'data-{j}'] = j

                children = (VElement(tag_name='p') for _ in range(0, 10))
                #for j in range(0, 10):
                #    children.append()

                el = VElement(tag_name='custom-element', xml_attrs=attrs, children=children)
            end = datetime.datetime.now()
            print('    Ended: ', end)
            print('    Duration: ', (end - start).total_seconds(), 'seconds')

            print('Parse XML with native functions: ')
            start = datetime.datetime.now()
            print('    Started: ', start)
            parser = window.DOMParser.new()
            html = parser.parseFromString(xml, 'text/html')
            print(f'    {len(html.body.children)} children parsed')
            end = datetime.datetime.now()
            print('    Ended: ', end)
            print('    Duration: ', (end - start).total_seconds(), 'seconds')


Output:

.. code-block::

     Creating 10000 elements with native createElement function

         Started:  2020-12-22 11:09:20.453000
         Ended:  2020-12-22 11:09:22.271000
         Duration:  1.818 seconds

     Creating 10000 elements with VElement initialization

         Started:  2020-12-22 11:09:22.279000
         Ended:  2020-12-22 11:09:24.201000
         Duration:  1.922 seconds

     Parse XML with native functions:

         Started:  2020-12-22 11:09:24.391000
         10000 children parsed
         Ended:  2020-12-22 11:09:24.794000
         Duration:  0.403 seconds
