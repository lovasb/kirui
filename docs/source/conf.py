from pathlib import Path

project = 'kirUI'
copyright = '2020, Bence Lovas'
author = 'Bence Lovas'

with (Path(__file__).parents[2] / 'VERSION').open('r', encoding='utf-8') as f:
    version = f.readline().strip()

release = version


# -- General configuration ---------------------------------------------------
extensions = [
    'sphinx_rtd_theme'
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

html_js_files = [
    'js/matomo.js'
]
