
Welcome to kirUI's documentation!
=================================

.. include:: README.rst

.. include:: measurement.rst

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
