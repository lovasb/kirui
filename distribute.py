import os
from pathlib import Path

for path in Path('django_kirui/static/django_kirui').glob('*'):
    path.unlink()

# os.system('parcel build parcel.html -d django_kirui/static/django_kirui/ --public-url /static/django_kirui/')
os.system('npx webpack --config webpack-build.config.js')

Path('django_kirui/static/django_kirui/style.js').unlink()
Path('django_kirui/static/django_kirui/style.css').rename('django_kirui/static/django_kirui/kirui.css')
Path('django_kirui/static/django_kirui/script.js').rename('django_kirui/static/django_kirui/kirui.js')
"""for path in Path('django_kirui/static/django_kirui').glob('parcel*'):
    if path.suffix == '.html':
        continue

    name = 'kirui' + ''.join(_ for _ in path.suffixes[1:])
    path.rename(path.parent / name)


for path in Path('django_kirui/static/django_kirui').glob('bootstrap*css*'):
    path.unlink()"""

os.system('python setup.py sdist upload')
