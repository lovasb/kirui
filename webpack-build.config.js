const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = {
  entry: {
    script: './kirui/index.js',
    style: './assets/scss/bootstrap.scss'
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'django_kirui/static/django_kirui'),
  },
  mode: "production",
  plugins: [new MiniCssExtractPlugin({filename: '[name].css'})],
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.scss$/,
        use: [
            MiniCssExtractPlugin.loader,
            "css-loader",
            "sass-loader"
        ],
      }
    ]
  },
  resolve: {
    alias: {
     'react': 'preact/compat',
     'react-dom': 'preact/compat',
    },
  }
};
