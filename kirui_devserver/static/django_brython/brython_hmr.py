import importlib
import sys

from browser import document, window


def on_reload_brython_module(evt):
    try:
        module = sys.modules[evt.detail['module_name']]

        data = {
            'detail': {
                'module_name': evt.detail['module_name']
            }
        }
        document.dispatchEvent(window.CustomEvent.new("reload_brython_module@before", data))
        importlib.reload(module)

        document.dispatchEvent(window.CustomEvent.new("reload_brython_module@after", data))
    except KeyError:  # this module is not imported by client
        return


document.bind('reload_brython_module', on_reload_brython_module)
