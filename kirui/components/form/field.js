import { Component } from "/kirui/core/component";
import { registry } from "/kirui/core/registry";
import { h } from 'preact';


class FormField extends Component {
    constructor(props) {
        super(props);
    }

    doRender() {
        let label;
        if (this.props.label) {
            let cls = 'col-form-label'
            if (this.props['label-width']) {
                for (let part of this.props['label-width'].split(' ')) {
                    cls += ' col-' + part;
                }
                delete this.props['label-width']
            }

            if (this.props.required) {
                cls += ' required';
            }
            delete this.props.required;

            let readonly = false;
            if (this.props.disabled) {
                readonly = true;
            }

            label = <label for={this.props.id} className={cls} readonly={readonly}>{this.props.label}</label>;
            delete this.props.label;
        }

        let cls = '';
        if (this.props['field-width']) {
            for (let part of this.props['field-width'].split(' ')) {
                cls += ' col-' + part;
            }
            delete this.props['field-width']
        } else {
            cls = 'col'
        }

        if (this.props.widget === undefined) {
            return <div></div>
        }
        let el = registry.getComponent(this.props.widget);
        el = h(...[el, this.props, this.props.children]);
        delete this.props.widget;

        return <div class="mb-3 row">
            {label}
            <div className={cls}>
                {el}
            </div>
        </div>
    }
}

registry.register('kr-form-field', FormField);
export { FormField }