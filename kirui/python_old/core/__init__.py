from .minify import minify
from .components import Component
from .elements import Element
from .render import render
from .diff import patch_dom_with_vdom